import os

def getAndReturnFileList():
    names = os.listdir()
    try:
        names.pop(names.index('.git'))
        names.pop(names.index('.gitignore'))
    except ValueError:
        pass
    names.pop(names.index('renamer.py'))
    return names

names = getAndReturnFileList()

for i in range(0, len(names)):
    tempI = i
    extension = str(names[i]).split(".")[-1]
    os.rename(names[i], f'{i}temp.{extension}')

names = getAndReturnFileList()

for i in range(0, len(names)):
    tempI = i
    fileExtension = names[i].split('.')[-1]
    os.rename(names[i], f'{tempI+1}.{fileExtension}')
print('Renaming Completed')
